package com.juancopara.laberinto;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.juancopara.laberinto.Util.Util;

import static com.juancopara.laberinto.Util.Util.*;

public class MainActivity extends Activity {

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("storage", String.valueOf(isExternalStorageWritable()));
        initComponents();
        sharedPreferences = getSharedPreferences(PREFERENCES_FILE_NAME, this.MODE_PRIVATE);
    }

    private void initComponents(){
        Button btnfacil = this.findViewById(R.id.btnfacil);
        Button btnintermedio = this.findViewById(R.id.btnintermedio);
        Button btndificil = this.findViewById(R.id.btndificil);
        Button btnextremo = this.findViewById(R.id.btnextreme);

        btnfacil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectLevel(LEVEL_EASY);
            }
        });

        btnintermedio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectLevel(LEVEL_MEDIUM);
            }
        });

        btndificil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectLevel(LEVEL_HARD);
            }
        });
        btnextremo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectLevel(LEVEL_EXTREME);
            }
        });
    }

    private void selectLevel(int idlevel){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("LAST_LEVEL",idlevel);
        editor.apply();
        Intent intent = new Intent(this, BoardActivity.class);
        intent.putExtra("LEVEL", idlevel);
        startActivity(intent);
    }



}