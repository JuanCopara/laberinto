package com.juancopara.laberinto;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.juancopara.laberinto.Util.Util.DIRECTION_DOWN;
import static com.juancopara.laberinto.Util.Util.DIRECTION_LEFT;
import static com.juancopara.laberinto.Util.Util.DIRECTION_RIGHT;
import static com.juancopara.laberinto.Util.Util.DIRECTION_UP;
import static com.juancopara.laberinto.Util.Util.LEVEL_EASY;
import static com.juancopara.laberinto.Util.Util.LEVEL_EASY_BOARD;
import static com.juancopara.laberinto.Util.Util.LEVEL_EASY_ENEMIES;
import static com.juancopara.laberinto.Util.Util.LEVEL_EASY_TIME;
import static com.juancopara.laberinto.Util.Util.LEVEL_EXTREME;
import static com.juancopara.laberinto.Util.Util.LEVEL_EXTREME_BOARD;
import static com.juancopara.laberinto.Util.Util.LEVEL_EXTREME_ENEMIES;
import static com.juancopara.laberinto.Util.Util.LEVEL_EXTREME_TIME;
import static com.juancopara.laberinto.Util.Util.LEVEL_HARD;
import static com.juancopara.laberinto.Util.Util.LEVEL_HARD_BOARD;
import static com.juancopara.laberinto.Util.Util.LEVEL_HARD_ENEMIES;
import static com.juancopara.laberinto.Util.Util.LEVEL_HARD_TIME;
import static com.juancopara.laberinto.Util.Util.LEVEL_MEDIUM;
import static com.juancopara.laberinto.Util.Util.LEVEL_MEDIUM_BOARD;
import static com.juancopara.laberinto.Util.Util.LEVEL_MEDIUM_ENEMIES;
import static com.juancopara.laberinto.Util.Util.LEVEL_MEDIUM_TIME;
import static com.juancopara.laberinto.Util.Util.PREFERENCES_FILE_NAME;


public class BoardActivity extends Activity {

    private TextView txtmessage;
    private TextView txtcounter;
    private GridLayout gytboard;
    private Button btnup;
    private Button btnleft;
    private Button btnright;
    private Button btndown;
    private int level;
    private long maxTime;
    private long remaining_time;
    private int[][] board;
    private int playerPosX = 0;
    private int playerPosY = 0;
    private PlaceView[] placeViews;
    private boolean[][] enemiesPos;
    private int enemiesCount;
    private boolean playerAlive = true;
    private CountDownTimer countDownTimer;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_board);
        initComponent();
        level = getIntent().getIntExtra("LEVEL",0);

        configureLevel();
        sharedPreferences = getSharedPreferences(PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);

    }

    private void initComponent(){
        txtmessage = this.findViewById(R.id.txtmessage);
        txtcounter = this.findViewById(R.id.txtcounter);
        gytboard = this.findViewById(R.id.gytboard);
        btndown = this.findViewById(R.id.btndown);
        btnup = this.findViewById(R.id.btnup);
        btnright = this.findViewById(R.id.btnright);
        btnleft = this.findViewById(R.id.btnleft);

        btndown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                movePlayer(DIRECTION_DOWN);
            }
        });

        btnleft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                movePlayer(DIRECTION_LEFT);
            }
        });

        btnright.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                movePlayer(DIRECTION_RIGHT);
            }
        });

        btnup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                movePlayer(DIRECTION_UP);
            }
        });
    }

    private void configureLevel(){
        txtcounter.setText("0");
        switch (level){
            case LEVEL_EASY:
                enemiesCount = LEVEL_EASY_ENEMIES;
                txtmessage.setText("Nivel facil");
                board = LEVEL_EASY_BOARD;
                maxTime = LEVEL_EASY_TIME;
                break;
            case LEVEL_MEDIUM:
                enemiesCount = LEVEL_MEDIUM_ENEMIES;
                txtmessage.setText("Nivel intermedio");
                board = LEVEL_MEDIUM_BOARD;
                maxTime = LEVEL_MEDIUM_TIME;
                break;
            case LEVEL_HARD:
                enemiesCount = LEVEL_HARD_ENEMIES;
                txtmessage.setText("Nivel dificil");
                board = LEVEL_HARD_BOARD;
                maxTime = LEVEL_HARD_TIME;
                break;
            case LEVEL_EXTREME:
                enemiesCount = LEVEL_EXTREME_ENEMIES;
                txtmessage.setText("Nivel Extremo");
                board = LEVEL_EXTREME_BOARD;
                maxTime = LEVEL_EXTREME_TIME;
                break;

            default:
                System.out.println("error");
                break;
        }
        fillEnemies();
        buildChess();
        setTimer();
    }

    private void buildChess(){
        placeViews = null;
        gytboard.removeAllViews();
        gytboard.setRowCount(board.length);
        gytboard.setColumnCount(board[0].length);
        placeViews = new PlaceView[board.length * board[0].length];

        for(int row = 0 ; row< board.length; row++){
            for(int col = 0 ; col < board[0].length;col++){

                boolean player = false;
                boolean enemie = false;
                boolean meta = false;

                //valida posicion jugador
                player = col == playerPosX && row == playerPosY;
                //valida posicion enemigo
                enemie = enemiesPos[row][col];
                //valida meta
                if (player && row == board.length-1 &&col == board[0].length-1) {
                    txtmessage.setText("Ganaste");
                    playerAlive = false;
                    countDownTimer.cancel();
                }
                if (row == board.length-1 &&col == board[0].length-1) {
                    meta = true;
                }

                PlaceView placeView = new PlaceView(this,enemie, player,meta, row,col);
                placeViews[row*board[0].length+col] = placeView;
                gytboard.addView(placeView);

            }

        }

        gytboard.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        final int MARGIN = 5;

                        int pWidth = gytboard.getWidth();
                        int pHeight = gytboard.getHeight();
                        int numOfCol = gytboard.getColumnCount();
                        int numOfRow = gytboard.getRowCount();
                        int w = pWidth/numOfCol;
                        int h = pHeight/numOfRow;

                        for(int yPos=0; yPos<numOfRow; yPos++){
                            for(int xPos=0; xPos<numOfCol; xPos++){
                                GridLayout.LayoutParams params =
                                        (GridLayout.LayoutParams)placeViews[yPos*numOfCol + xPos].getLayoutParams();
                                params.width = w - 2*MARGIN;
                                params.height = h - 2*MARGIN;
                                params.setMargins(MARGIN, MARGIN, MARGIN, MARGIN);
                                placeViews[yPos*numOfCol + xPos].setLayoutParams(params);
                            }
                        }
                    }
                }
        );

    }

    private ImageButton buildPlace(){
        ImageButton place = new ImageButton(this);
        place.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.MATCH_PARENT));
        place.setBackgroundColor(Color.parseColor("#ffffff"));
        return place;
    }

    private void setTimer(){
        long diff = 1000;
        countDownTimer = new CountDownTimer(maxTime, diff){

            @Override
            public void onTick(long millisUntilFinished) {
                long diff = maxTime - (maxTime - millisUntilFinished);
                remaining_time = maxTime - (maxTime - millisUntilFinished);
                txtcounter.setText(String.valueOf(remaining_time/1000));
            }

            @Override
            public void onFinish() {
                txtmessage.setText("Perdiste");
                playerAlive = false;
            }
        }.start();

    }

    private void movePlayer(int direction){

        if(playerAlive) {
            switch (direction) {
                case DIRECTION_DOWN:
                    if (playerPosY + 1 < board[0].length && playerPosY + 1 >= 0) {
                        playerPosY++;
                        buildChess();
                    }
                    break;
                case DIRECTION_LEFT:
                    if (playerPosX - 1 < board.length && playerPosX - 1 >= 0) {
                        playerPosX--;
                        buildChess();
                    }
                    break;
                case DIRECTION_RIGHT:
                    if (playerPosX + 1 < board.length && playerPosX + 1 >= 0) {
                        playerPosX++;
                        buildChess();
                    }
                    break;
                case DIRECTION_UP:
                    if (playerPosY - 1 < board[0].length && playerPosY - 1 >= 0) {
                        playerPosY--;
                        buildChess();
                    }
                    break;
            }
        }


    }

    public void nuke(){
        System.out.println( ":v");
        playerAlive = false;
        txtmessage.setText("Perdiste");
        countDownTimer.cancel();

        SharedPreferences.Editor editor =sharedPreferences.edit();
        editor.putBoolean("PENDING_GAME", false);
        editor.apply();
    }
    private void fillEnemies(){
        List<String> enemiesCord = new ArrayList<>();
        enemiesPos = new boolean[board.length][board[0].length];
        Random random = new Random();
//        for(int x = 0; x<enemiesPos.length;x++){
//            for(int y = 0; y<enemiesPos[0].length;y++){
//
//                if(!(x == 0 && y==0 || x==board.length && y==board[0].length))
//                    enemiesPos[x][y] = random.nextBoolean();
//
//            }
//
//        }
        for(int aux = 0; aux < enemiesCount; aux++){
            boolean availablepos = false;
            while(!availablepos){
                int x = random.nextInt((board.length));
                int y = random.nextInt((board[0].length));
                String posiblepos = x +String.valueOf(y);
                if(!(x == 0 && y==0)) {
                    if (x == board.length-1 && y == board[0].length-1) {
                        availablepos = false;
                    }
                    else if(enemiesCord.contains(posiblepos)){
                        availablepos=false;
                    }
                    else{
                        enemiesCord.add(posiblepos);
                        availablepos = true;
                        enemiesPos[x][y] = true;
                    }

                }

            }

        }

    }

    private void saveGame(){
        SharedPreferences.Editor editor =sharedPreferences.edit();
        editor.putBoolean("PENDING_GAME", true);
        editor.putLong("REMAINING_TIME", remaining_time);
        editor.putInt("PLAYER_POSITION_X", playerPosX);
        editor.putInt("PLAYER_POSITION_Y", playerPosY);
        editor.apply();
        countDownTimer.cancel();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if(sharedPreferences.getBoolean("PENDING_GAME",false)){
            playerPosX = sharedPreferences.getInt("PLAYER_POSITION_X",0);
            playerPosY = sharedPreferences.getInt("PLAYER_POSITION_Y",0);
            maxTime = sharedPreferences.getLong("REMAINING_TIME", maxTime);
            buildChess();
            setTimer();
        }
    }



    @Override
    protected void onPause() {
        super.onPause();
        saveGame();

    }

}