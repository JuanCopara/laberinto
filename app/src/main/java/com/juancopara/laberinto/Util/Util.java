package com.juancopara.laberinto.Util;

import android.os.Environment;

public class Util {

    public static final int LEVEL_EASY = 1;
    public static final int LEVEL_MEDIUM = 2;
    public static final int LEVEL_HARD = 3;
    public static final int LEVEL_EXTREME = 4;

    public static final int[][] LEVEL_EASY_BOARD = new int[4][4];
    public static final int[][] LEVEL_MEDIUM_BOARD = new int[6][6];
    public static final int[][] LEVEL_HARD_BOARD = new int[8][8];
    public static final int[][] LEVEL_EXTREME_BOARD = new int[16][16];

    public static final int LEVEL_EASY_ENEMIES = 5;
    public static final int LEVEL_MEDIUM_ENEMIES = 9;
    public static final int LEVEL_HARD_ENEMIES = 20;
    public static final int LEVEL_EXTREME_ENEMIES = 60;

    public static final long LEVEL_EASY_TIME = 10000;
    public static final long LEVEL_MEDIUM_TIME = 15000;
    public static final long LEVEL_HARD_TIME = 17000;
    public static final long LEVEL_EXTREME_TIME = 20000;

    public static final int DIRECTION_UP = 0;
    public static final int DIRECTION_DOWN = 1;
    public static final int DIRECTION_RIGHT = 2;
    public static final int DIRECTION_LEFT = 3;

    public static final String PREFERENCES_FILE_NAME = "laberinto_preferences";


    public static boolean isExternalStorageWritable(){
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }
}
