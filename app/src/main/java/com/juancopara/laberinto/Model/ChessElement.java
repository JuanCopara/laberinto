package com.juancopara.laberinto.Model;

import android.graphics.drawable.Drawable;
import android.widget.ImageButton;

public class ChessElement {

    private int pos_current;
    private int pos_correct;
    private boolean enemy;
    private ImageButton imageButton;
    private Drawable drawable;


    public ChessElement(int pos_current, int pos_correct, boolean enemy, ImageButton imageButton, Drawable drawable) {
        this.pos_current = pos_current;
        this.pos_correct = pos_correct;
        this.enemy = enemy;
        this.imageButton = imageButton;
        this.drawable = drawable;
    }

    public int getPos_current() {
        return pos_current;
    }

    public void setPos_current(int pos_current) {
        this.pos_current = pos_current;
    }

    public int getPos_correct() {
        return pos_correct;
    }

    public void setPos_correct(int pos_correct) {
        this.pos_correct = pos_correct;
    }

    public boolean isEnemy() {
        return enemy;
    }

    public void setEnemy(boolean enemy) {
        this.enemy = enemy;
    }

    public ImageButton getImageButton() {
        return imageButton;
    }

    public void setImageButton(ImageButton imageButton) {
        this.imageButton = imageButton;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }
}
