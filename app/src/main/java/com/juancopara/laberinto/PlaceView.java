package com.juancopara.laberinto;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;


public class PlaceView extends View {

    private boolean enemie = false;
    private boolean player = false;
    private boolean meta = false;
    private int idx = 0;
    private int idy= 0;
    private Context context;

    public PlaceView(Context context) {
        super(context);
    }


    public PlaceView(Context context, boolean enemie, boolean player,boolean meta, int x, int y) {
        super(context);
        this.context = context;
        this.enemie = enemie;
        this.player = player;
        this.meta = meta;
        this.idx = x;
        this.idy = y;
    }

    public PlaceView(Context context, int idx, int idy) {
        super(context);
        this.idx = idx;
        this.idy = idy;
    }

    public PlaceView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if(enemie && player){
            Drawable drawable = getResources().getDrawable(R.drawable.ic_skull, null);
            drawable.setBounds(0,0, canvas.getWidth(),canvas.getHeight());
            drawable.draw(canvas);
            ((BoardActivity) context).nuke();
        }
        else if(enemie){

            Drawable drawable = getResources().getDrawable(R.drawable.ic_bomb);
            drawable.setBounds(0,0, canvas.getWidth(),canvas.getHeight());
            drawable.draw(canvas);
        }
        else if(player){
            canvas.drawColor(Color.GREEN);
        }else if(meta){
            Drawable drawable = getResources().getDrawable(R.drawable.ic_treasure,null);
            drawable.setBounds(0,0, canvas.getWidth(),canvas.getHeight());
            drawable.draw(canvas);
        }else{
            canvas.drawColor(Color.YELLOW);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec),
                MeasureSpec.getSize(heightMeasureSpec));
    }

    public boolean isEnemie() {
        return enemie;
    }

    public void setEnemie(boolean enemie) {
        this.enemie = enemie;
    }

    public boolean isPlayer() {
        return player;
    }

    public void setPlayer(boolean player) {
        this.player = player;
    }

    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    public int getIdy() {
        return idy;
    }

    public void setIdy(int idy) {
        this.idy = idy;
    }
}
